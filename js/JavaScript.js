
document.querySelector('.hamburger').addEventListener('click', function(){

    document.querySelector('nav').classList.toggle('open');

} );

$('.b1').on('click', function () {
  $('.c1').show("1000");
  $(this).hide(); 
  $('.b2').show()
  $('.b3').show() 
})

$('.b1').on('click', function () {
  $('.c2').hide();
  $('.c3').hide();
})

$('#close').on('click', function () {
  $('.center').hide("1000");
  $('.button-1').show();
})

$('.b2').on('click', function () {
  $('.c2').show("1000");
  $(this).hide();
  $('.b1').show()
  $('.b3').show()
})

$('.b2').on('click', function () {
  $('.c1').hide();
  $('.c3').hide();
})

$('#close-2').on('click', function () {
  $('.c2').hide("1000");
  $('.b2').show();
})


$('.b3').on('click', function () {
  $('.c3').show("1000");
  $(this).hide();
  $('.b1').show()
  $('.b2').show() 
})

$('.b3').on('click', function () {
  $('.c1').hide();
  $('.c2').hide();
})

$('#close-3').on('click', function () {
  $('.c3').hide("1000");
  $('.b3').show();
})

$('.slider-container').slick({
	//autoplay: false,//					//Do you want it to autoplay? true or false
	//autoplaySpeed: 2000,//		//How long between each slide when autoplaying?
	speed: 1000,							//How fast is the transition?
	arrows: true,						//Do you want to show arrows to trigger each slide?
	accessibility: true,		//Enables tabbing and arrow key navigation
	dots: true,							//Enables the  dots below to show how many slides
	fade: false,						//Changes the animate from slide to fade if true
	infinite: true, 				//When true, means that it will scroll in a circle
	pauseOnHover: false,		//When true means the autoplay pauses when hovering
	pauseOnDotsHover: true, //Pause the autoplay when hovering over the dots
	slidesToShow: 3,
	slidesToScroll: 1,
  prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
  nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>',
	responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
  ]
});
				

$('.question').click( function() {
    console.log( $(this).attr('data-ans') )
  
  var dataAns = $(this).attr('data-ans');
  
  $('.answer').not('.' +dataAns).slideUp();
    
  $('.' +dataAns).slideToggle();
  
  });

$('.read-more-content').addClass('hide')
$('.read-more-show, .read-more-hide').removeClass('hide')

$('.read-more-show').on('click', function(e) {
  $(this).next('.read-more-content').removeClass('hide');
  $(this).addClass('hide');
  e.preventDefault();
});

$('.read-more-hide').on('click', function(e) {
  $(this).parent('.read-more-content').addClass('hide');
  var moreid=$(this).attr("more-id");
  $('.read-more-show#'+moreid).removeClass('hide');
  e.preventDefault();
});


  
